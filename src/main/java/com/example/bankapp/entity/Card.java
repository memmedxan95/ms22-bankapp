package com.example.bankapp.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.util.*;

@Entity
@Table(name = "cards")
@Data
@NamedQueries({
        @NamedQuery(name = "findByCardNumber",
                query = "select c from Card c where c.cardNumber =:cardNum"
        ),
        @NamedQuery(name = "findBycardType",
                query = "select c from Card c where c.cardType =:cardType"
        ),
        @NamedQuery(name = "findCardNumberByCardType",
        query = "select c.cardNumber from Card c where c.cardType=:cardType")
})
public class Card {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "card_id")
    private Long id;

    @ManyToOne  // done
    @JoinColumn(name = "account_id", nullable = false)
    @ToString.Exclude
    @JsonIgnore
    private Account account;

    @Column(name = "contract_number")
    private String cardNumber;

    @Column(name = "card_type")
    private String cardType;

    @Column(name = "expiration_date")
    private String expirationDate;

    @Column(name = "card_benefits")
    private String cardBenefits;

    @OneToMany(mappedBy = "card", cascade = CascadeType.ALL)// done
    private Set<CardBenefit> benefits;



}