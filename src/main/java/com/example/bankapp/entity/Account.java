package com.example.bankapp.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import lombok.ToString;

import java.util.List;
import java.util.Set;
@Data
@Entity
@Table(name = "accounts")
@NamedEntityGraph(name = "account-user" , attributeNodes = {
        @NamedAttributeNode("user"),
        @NamedAttributeNode("cards"),
        @NamedAttributeNode(value = "cards" , subgraph = "subgraph.card.benefits")
}, subgraphs = {
        @NamedSubgraph(name = "subgraph.card.benefits",
                attributeNodes = @NamedAttributeNode(value = "benefits") )
})
@NamedQuery(name ="findAccountDto", query = "select new com.example.bankapp.dto.AccountDto(a.accountNumber, a.balance, c.cardNumber, c.cardType, u.username, u.password) " +
        "from Account a join a.cards c join a.user u")
@NamedQuery(name = "findByTransion", query = "select a from Account a join a.fromTransactions")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "account_id")
    private Long id;

    @ManyToOne // done
    @JoinColumn(name="user_id", nullable = false)
//    @ToString.Exclude
//    @JsonIgnore
    private User user;

    private String accountNumber;
    private double balance;

    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL, fetch = FetchType.EAGER)  // done
    private List<Card> cards;

    @OneToMany(mappedBy = "fromAccount")
    @ToString.Exclude
    @JsonIgnore
    private Set<Transaction> fromTransactions;

    @OneToMany(mappedBy = "toAccount")
    @ToString.Exclude
    @JsonIgnore
    private Set<Transaction> toTransactions;


}