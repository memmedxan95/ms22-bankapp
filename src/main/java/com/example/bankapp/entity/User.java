package com.example.bankapp.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import org.apache.commons.lang3.builder.HashCodeExclude;

import java.util.List;
import java.util.Set;

@Data
@Entity
@Table(name = "users")
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long id;

    @Column(unique = true, name = "user_name")
    private String username;

    private String password;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL) // done
    @ToString.Exclude
    @JsonIgnore
    private List<Account> account;

    @OneToOne(mappedBy = "user") // done
    private Address address;
}