package com.example.bankapp.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import lombok.ToString;
import org.springframework.web.bind.annotation.CookieValue;

import java.time.LocalDate;

@Entity
@Table(name = "transactions")
@Data
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transaction_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name="from_account_id", nullable=false,insertable=false, updatable=false)
    @ToString.Exclude
    @JsonIgnore
    private Account fromAccount;

    @ManyToOne
    @JoinColumn(name="to_account_id", nullable=false,insertable=false, updatable=false)
    @ToString.Exclude
    @JsonIgnore
    private Account toAccount;

    private double amount;
    private LocalDate date;
    private String type; // Exp: "transfer", "withdraw", "deposit"
}