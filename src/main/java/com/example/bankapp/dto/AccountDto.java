package com.example.bankapp.dto;


import com.example.bankapp.entity.Card;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountDto {

    private String accountNumber;
    private double balance;

    private String cardNumber;
    private String cardType;

    private String username;
    private String password;

//    private List<Card> cards;

}
