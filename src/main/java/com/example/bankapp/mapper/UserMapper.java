package com.example.bankapp.mapper;

import com.example.bankapp.dto.UserDto;
import com.example.bankapp.entity.User;


public enum UserMapper {
    USER_MAPPER;

        public User mapToUser(UserDto userDto){
            return User.builder().
                    username(userDto.getUserName()).
                    password(userDto.getPassword()).build();


        }

        public UserDto mapToUserDto(User user){
            return UserDto.builder().
                    userName(user.getUsername()).
                    password(user.getPassword()).build();

        }
}
