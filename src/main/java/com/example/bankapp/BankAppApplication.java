package com.example.bankapp;

import com.example.bankapp.dto.UserDto;
import com.example.bankapp.entity.Card;
import com.example.bankapp.repository.AccountRepository;
import com.example.bankapp.repository.CardRepository;
import com.example.bankapp.repository.UserRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class BankAppApplication implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private CardRepository cardRepository;

    public static void main(String[] args) {
        SpringApplication.run(BankAppApplication.class, args);
    }

    @Transactional
    @Override
    public void run(String... args) throws Exception {

      accountRepository.findAccountDto().forEach(System.out::println);



    }
}
