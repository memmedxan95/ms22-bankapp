package com.example.bankapp.repository;

import com.example.bankapp.dto.AccountDto;
import com.example.bankapp.entity.Account;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {



    @Query("select a from Account a join fetch a.fromTransactions f")
    Optional<Account> findCustom();


    @Query(name = "findAccountDto")
    List<AccountDto> findAccountDto();

    @EntityGraph(attributePaths = {"user", "cards","cards.benefits"})
    List<Account> findAccountBy();





}
