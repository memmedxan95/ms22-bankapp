package com.example.bankapp.repository;

import com.example.bankapp.entity.Card;
import com.example.bankapp.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;


public interface CardRepository extends JpaRepository<Card, Long> {




//    @Query(value = "SELECT * FROM cards c " +
//            "LEFT JOIN accounts a ON c.card_id = a.account_id " , nativeQuery = true)
//    List<Card> findCustom3();


    @Query(name = "findByCardNumber")
    List<Card> findCardByCardNumber(String cardNum);

    @Query(name = "findBycardType")
    List<Card> findCardByCardType(String cardType);

    @Query(name = "findCardNumberByCardType")
    String cardNumberbyType(String cardType);



}