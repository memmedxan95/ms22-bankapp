package com.example.bankapp.repository;

import com.example.bankapp.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
//    Optional<User> findByUsernameOrIdAndPasswordOrAddress(String username, Long id);



    @Query("SELECT u FROM User u WHERE u.username like %:name")
    Optional<User> findUserByUsernameContaining(@Param("name") String  name);

//    @Query("select a from User a join fetch a.address b join fetch a.account c")
//    Optional<User> findCustom();


    @Query(value = "SELECT * FROM users a " +
            "JOIN addresses b ON a.user_id = b.address_id " +
            "JOIN accounts c ON a.user_id = c.account_id", nativeQuery = true)
    Optional<User> findCustom2();






}
