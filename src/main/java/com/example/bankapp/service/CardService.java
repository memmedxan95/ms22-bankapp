package com.example.bankapp.service;

import com.example.bankapp.entity.Card;
import org.springframework.stereotype.Service;

@Service
public interface CardService {

    Card getCard(Long id);
}
