package com.example.bankapp.service;


import com.example.bankapp.dto.UserDto;
import com.example.bankapp.entity.User;
import com.example.bankapp.repository.UserRepository;
import org.springframework.stereotype.Service;


@Service
public interface UserService {


    User getUser(Long id);

    public User getUsername(String name);

    public UserDto getUserById(Long id);


}
