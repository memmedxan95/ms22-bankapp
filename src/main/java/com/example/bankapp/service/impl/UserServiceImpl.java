package com.example.bankapp.service.impl;

import com.example.bankapp.dto.UserDto;
import com.example.bankapp.entity.User;
import com.example.bankapp.repository.UserRepository;
import com.example.bankapp.service.UserService;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import static com.example.bankapp.mapper.UserMapper.USER_MAPPER;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public User getUser(Long id) {

        User user = null;
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isPresent()) {
            user = userOptional.get();
        }
        return user;
    }



    @Override
    public User getUsername(String name) {

        User user = null;
        Optional<User> userOptional = userRepository.findUserByUsernameContaining(name);
        if (userOptional.isPresent()) {
            user = userOptional.get();
        }
        return user;


    }

    @Override
    public UserDto getUserById(Long id) {
        return null;
    }


}
