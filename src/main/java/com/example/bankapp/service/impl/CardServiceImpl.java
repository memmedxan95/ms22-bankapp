package com.example.bankapp.service.impl;

import com.example.bankapp.entity.Card;
import com.example.bankapp.entity.User;
import com.example.bankapp.repository.CardRepository;
import com.example.bankapp.service.CardService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CardServiceImpl implements CardService {


    private final CardRepository cardRepository;
    @Override
    public Card getCard(Long id) {

        Card card = null;
        Optional<Card> cardOptional = cardRepository.findById(id);
        if (cardOptional.isPresent()) {
            card = cardOptional.get();
        }
        return card;

}
    }