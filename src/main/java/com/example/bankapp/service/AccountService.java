package com.example.bankapp.service;

import com.example.bankapp.entity.Account;

public interface AccountService {
    Account getAccount(Long id);
}
