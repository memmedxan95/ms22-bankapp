package com.example.bankapp.controller;


import com.example.bankapp.entity.Card;
import com.example.bankapp.service.CardService;
import com.example.bankapp.service.impl.CardServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/card/")
public class CardController {

        private final CardServiceImpl cardServiceImpl;

    @GetMapping("{id}")
    public ResponseEntity<Card> getCard(@PathVariable Long id) {
        return ResponseEntity.ok(cardServiceImpl.getCard(id));
    }

}
