package com.example.bankapp.controller;

import com.example.bankapp.dto.UserDto;
import com.example.bankapp.entity.User;
import com.example.bankapp.mapper.UserMapper;
import com.example.bankapp.service.UserService;
import com.example.bankapp.service.impl.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user/")
public class UserController {


    private final UserServiceImpl userServiceImpl;

    @GetMapping("/{id}")
    public ResponseEntity<User> getUser(@PathVariable Long id) {
        return ResponseEntity.ok(userServiceImpl.getUser(id));
    }

    @GetMapping("/4")
    public ResponseEntity<User> getUsername() {
        return ResponseEntity.ok(userServiceImpl.getUsername("xan"));
    }


}
